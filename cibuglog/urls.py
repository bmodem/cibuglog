"""cibuglog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.urls import path
from rest_framework import routers
from django.contrib import admin
from django.conf import settings
from django.views.generic.base import RedirectView

from CIResults import views, rest_views, metrics_views, bugs_views
from CIResults.models import Test, Machine

router = routers.DefaultRouter()
router.register(r'build', rest_views.BuildViewSet)
router.register(r'component', rest_views.ComponentViewSet)
router.register(r'issuefilter', rest_views.IssueFilterViewSet)
router.register(r'runconfig', rest_views.RunConfigViewSet)
router.register(r'test', rest_views.TestSet)
router.register(r'bugtrackeraccount', rest_views.BugTrackerAccountViewSet) # WARNING: Hard-coded URL in helpers.js

base_patterns = [
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('allauth.urls')),

    url(r'^api/metrics/passrate$', rest_views.metrics_passrate_per_runconfig_view, name="api-metrics-passrate"),  # NOTE: Kept for historical reasons
    url(r'^api/metrics/passrate_per_runconfig$', rest_views.metrics_passrate_per_runconfig_view, name="api-metrics-passrate-per-runconfig"),
    url(r'^api/metrics/passrate_per_test$', rest_views.metrics_passrate_per_test_view, name="api-metrics-passrate-per-test"),
    url(r'^api/metrics$', metrics_views.api_metrics),  # WARNING: counts private tests, machines and issues
    url(r'^api/', include((router.urls, "CIResults"), namespace='api')),

    url(r'^$', views.index, name='CIResults-index'),

    # ReplicationScript
    path('bug/replication/', bugs_views.ReplicationScriptListView.as_view(),
         name='CIResults-replication-script-list'),
    path('bug/replication/create', bugs_views.ReplicationScriptCreateView.as_view(),
         name='CIResults-replication-script-create'),
    path('bug/replication/dryrun', bugs_views.replication_script_check,
         name='CIResults-replication-script-check'),
    path('bug/replication/<pk>', bugs_views.ReplicationScriptEditView.as_view(),
         name='CIResults-replication-script-edit'),

    # Issues
    url(r'^issue/$', views.IssueListView.as_view(), name='CIResults-issues-list'),
    url(r'^issue/(?P<action>(create))', views.IssueView.as_view(), name='CIResults-issue'),
    url(r'^issue/(?P<pk>[0-9]+)(/|/history)?$', views.IssueDetailView.as_view(), name='CIResults-issue-detail'),
    url(r'^issue/(?P<pk>[0-9]+)/(?P<action>(edit|archive|restore|show|hide))$', views.IssueView.as_view(), name='CIResults-issue'),
    url(r'^issuefilterassoc/(?P<pk>[0-9]+)(/|/history)?$', views.IFADetailView.as_view(), name='CIResults-ifa-detail'),
    url(r'^issuefilter/(?P<action>(stats))$', views.IssueFilterView.as_view(), name='CIResults-issuefilter'),

    # Tests
    url(r'^test/$', views.TestListView.as_view(), name="CIResults-tests"),
    url(r'^test/(?P<pk>[0-9]+)(/|/history)?$', views.TestDetailView.as_view(), name='CIResults-test-detail'),
    url(r'^test/(?P<pk>[0-9]+)/edit$', views.TestEditView.as_view(), name='CIResults-test-edit'),
    url(r'^test/(?P<pk>[0-9]+)/rename$', views.TestRenameView.as_view(), name='CIResults-test-rename'),
    url(r'^test/(?P<pk>[0-9]+)/suppress$', views.object_suppress, name='CIResults-test-suppress',
        kwargs={"klass": Test, "permission": 'CIResults.suppress_test'}),
    url(r'^test/(?P<pk>[0-9]+)/vet$', views.object_vet, name='CIResults-test-vet',
        kwargs={"klass": Test, "permission": 'CIResults.vet_test'}),
    url(r'^test/mass-vetting$', views.TestMassVettingView.as_view(), name='CIResults-test-mass-vetting'),
    url(r'^tests/mass-rename$', views.TestMassRenameView.as_view(), name="CIResults-tests-massrename"),

    # Machines
    url(r'^machine/$', views.MachineListView.as_view(), name="CIResults-machines"),
    url(r'^machine/(?P<pk>[0-9]+)/edit$', views.MachineEditView.as_view(), name='CIResults-machine-edit'),
    url(r'^machine/(?P<pk>[0-9]+)/suppress$', views.object_suppress, name='CIResults-machine-suppress',
        kwargs={"klass": Machine, "permission": 'CIResults.suppress_machine'}),
    url(r'^machine/(?P<pk>[0-9]+)/vet$', views.object_vet, name='CIResults-machine-vet',
        kwargs={"klass": Machine, "permission": 'CIResults.vet_machine'}),
    url(r'^machine/(?P<pk>[0-9]+)(/|/history)?$', views.MachineDetailView.as_view(), name='CIResults-machine-detail'),
    url(r'^machine/mass-vetting$', views.MachineMassVettingView.as_view(), name='CIResults-machine-mass-vetting'),

    # Results
    url(r'^results/all', views.TestResultListView.as_view(), name="CIResults-results"),
    url(r'^results/knownfailures$', views.KnownFailureListView.as_view(), name="CIResults-knownfailures"),
    url(r'^results/compare', views.ResultsCompareView.as_view(), name="CIResults-compare"),

    # Bugs
    url(r'^bug/(?P<pk>[0-9]+)/flag-for-update', bugs_views.bug_flag_for_update, name="CIResults-bug-flag-for-update"),
    url(r'^bug/open_bugs$', bugs_views.open_bugs, name="CIResults-bug-open-list"),


    # Histories
    url(r'^testsuite/(?P<pk>[0-9]+)(/|/history)?$', views.TestSuiteDetailView.as_view(), name='CIResults-testsuite-detail'),
    url(r'^textstatus/(?P<pk>[0-9]+)(/|/history)?$', views.TextStatusDetailView.as_view(), name='CIResults-textstatus-detail'),
    url(r'^runcfg/(?P<pk>[0-9]+)(/|/history)?$', views.RunConfigDetailView.as_view(), name='CIResults-runcfg-detail'),
    url(r'^component/(?P<pk>[0-9]+)(/|/history)?$', views.ComponentDetailView.as_view(), name='CIResults-component-detail'),
    url(r'^build/(?P<pk>[0-9]+)(/|/history)?$', views.BuildDetailView.as_view(), name='CIResults-build-detail'),
    url(r'^runcfgtag/(?P<pk>[0-9]+)(/|/history)?$', views.RunConfigTagDetailView.as_view(), name='CIResults-runcfgtag-detail'),
    url(r'^testresult/(?P<pk>[0-9]+)(/|/history)?$', views.TestResultDetailView.as_view(), name='CIResults-testresult-detail'),

    # Vetting
    url(r'^textstatus/mass-vetting$', views.TextStatusMassVettingView.as_view(), name='CIResults-textstatus-mass-vetting'),

    # Metrics
    url(r'^metrics/issues', metrics_views.metrics_issues, name='CIResults-metrics-issues'),
    url(r'^metrics/bugs', metrics_views.metrics_bugs, name='CIResults-metrics-bugs'),
    url(r'^metrics/comments', metrics_views.metrics_comments, name="CIResults-metrics-comments"),
    url(r'^metrics/open_bugs$', RedirectView.as_view(pattern_name='CIResults-bug-open-list'), name="CIResults-metrics-open-bugs"),
    url(r'^metrics/passrate_per_runconfig$', metrics_views.metrics_passrate_per_runconfig, name='CIResults-metrics-passrate-per-runconfig'),
    url(r'^metrics/passrate_per_test$', metrics_views.metrics_passrate_per_test, name='CIResults-metrics-passrate-per-test'),
    url(r'^metrics/passrate$', metrics_views.metrics_passrate_per_runconfig, name='CIResults-metrics-passrate'),  # NOTE: Kept for historical reasons
    url(r'^metrics/runtime', metrics_views.metrics_runtime_history, name='CIResults-metrics-runtime'),
]

if settings.DEBUG:
    import debug_toolbar
    base_patterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + base_patterns

if settings.URL_PREFIX:
    urlpatterns = [url('^{}/'.format(settings.URL_PREFIX), include(base_patterns))]
else:
    urlpatterns = base_patterns
