from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.decorators import permission_required
from django.db import transaction
from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
from django.db.models import Prefetch
from django.utils import timezone
from django.views.decorators.http import require_http_methods, require_POST
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.urls import reverse

from collections import defaultdict

from .models import BugTracker, Bug
from .models import BugComment, ReplicationScript

from .sandbox.io import Client
from .serializers import ReplicationScriptSerializer

from datetime import timedelta
import json


@require_POST
@permission_required('CIResults.change_replicationscript')
def replication_script_check(request, **kwargs):
    script = request.POST.get("script")
    src_tracker_name = request.POST["source_tracker"]
    dest_tracker_name = request.POST["destination_tracker"]

    src_tracker = BugTracker.objects.get(name=src_tracker_name)
    dest_tracker = BugTracker.objects.get(name=dest_tracker_name)

    try:
        client = Client.get_or_create_instance(script)
    except ValueError as e:
        return JsonResponse({'status': 'false', 'message': str(e)}, status=500)
    except (OSError, IOError):
        msg = "Script failed, likely due to illegal syscall made by script."
        return JsonResponse({'status': 'false', 'message': msg}, status=500)

    try:
        p = Prefetch('parent_bugs', queryset=Bug.objects.filter(tracker=dest_tracker), to_attr="par_bugs")
        bug_list = Bug.objects.filter(tracker=src_tracker).prefetch_related(p)
        bugs = src_tracker.tracker.tracker_check_replication(bug_list, dest_tracker, script, client, dryrun=True)

    except Client.UserFunctionCallError as e:
        return JsonResponse({'status': 'false', 'message': e.reason}, status=500)
    except ValueError as e:
        return JsonResponse({'status': 'false', 'message': str(e)}, status=500)
    finally:
        client.shutdown()
    return JsonResponse({'bugs': bugs}, safe=False)


class ReplicationScriptListView(ListView):
    model = ReplicationScript
    context_object_name = 'script_list'

    def get_queryset(self):
        return ReplicationScript.objects.all()


class ReplicationScriptCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = ReplicationScript
    fields = ['name', 'enabled', 'source_tracker', 'destination_tracker', 'script']
    template_name = 'CIResults/replication_script.html'

    permission_required = 'CIResults.add_replicationscript'
    permission_denied_message = "You don't have the necessary permissions to create a replication script"

    def get_success_url(self):
        messages.success(self.request, "The script {} has been created".format(self.object))
        return reverse("CIResults-replication-script-list")

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super().form_valid(form)


class ReplicationScriptEditView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = ReplicationScript
    fields = ['name', 'enabled', 'source_tracker', 'destination_tracker', 'script']
    template_name = 'CIResults/replication_script.html'

    permission_required = 'CIResults.change_replicationscript'
    permission_denied_message = "You don't have the necessary permissions to edit a replication script"

    def get_success_url(self):
        messages.success(self.request, "The script {} has been edited".format(self.object))
        return reverse("CIResults-replication-script-list")

    def form_valid(self, form):
        original = ReplicationScript.objects.get(pk=self.object.pk)
        orig_hist = json.loads(form.instance.script_history)
        orig_hist.append(ReplicationScriptSerializer(original).data)
        form.instance.script_history = json.dumps(orig_hist)
        return super().form_valid(form)


def open_bugs(request, **kwargs):
    # Parse the user request
    user_query = Bug.from_user_filters(**request.GET.copy())
    if not user_query.is_valid:
        messages.error(request, "Filtering error: " + user_query.error)

    # If the user request is empty, just select everything
    selected_bugs = Bug.objects.all() if user_query.is_empty else user_query.objects
    selected_bugs = selected_bugs.filter(parent=None)
    selected_bugs = selected_bugs.defer('description')
    selected_bugs = selected_bugs.prefetch_related('tracker', 'assignee__person', "creator__person",
                                                   "issue_set", "parent_bugs")

    referenced_bugs = set([b.id for b in selected_bugs if len(b.issue_set.all()) > 0])
    all_followed_and_open_bugs = set([b for b in selected_bugs if b.is_open and (b.id in referenced_bugs or
                                                                                 b.component in b.tracker.components_followed_list or not b.tracker.tracker.has_components)])  # noqa
    all_comments = BugComment.objects.filter(bug__in=selected_bugs).only('bug', 'account', 'created_on')
    all_comments = all_comments.prefetch_related('bug__tracker', "account__person")

    # HACK: Preload all the comments and assign them to their respective bugs
    comments = defaultdict(list)
    for comment in all_comments:
        comments[(comment.bug_id, comment.bug.tracker_id)].append(comment)
    for bug in all_followed_and_open_bugs:
        bug.comments_cached = comments[(bug.id, bug.tracker_id)]

    # Now, classify the urgency
    overdue = list()
    close = list()
    other = list()
    for bug in sorted(all_followed_and_open_bugs, key=lambda b: b.effective_priority, reverse=True):
        time_left = bug.SLA_remaining_time
        if time_left < timedelta(seconds=0):
            overdue.append(bug)
        elif time_left < timedelta(days=7):
            close.append(bug)  # pragma: no cover (TODO: requires to control the current time)
        else:
            other.insert(-1, bug)

    context = {
        "trackers": BugTracker.objects.all(),
        "all_open_bugs": all_followed_and_open_bugs,
        "bugs_overdue": overdue,
        "bugs_close_to_deadline": close,
        "bugs_other": other,

        'filters_model': Bug,
        'query': user_query,
    }
    return render(request, 'CIResults/open_bugs.html', context)


@transaction.atomic
@require_http_methods(["POST"])
def bug_flag_for_update(request, pk):
    bug = get_object_or_404(Bug.objects.select_for_update(), pk=pk)

    if not bug.is_being_updated:
        bug.flagged_as_update_pending_on = timezone.now()
        bug.save()
        status = 200
    else:
        status = 409

    return JsonResponse(data={}, status=status)
