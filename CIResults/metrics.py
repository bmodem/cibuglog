from django.db.models import Sum, Max, Min, Avg
from django.utils import timezone
from django.utils.functional import cached_property

from .models import Issue, Bug, BugComment, BugTracker, KnownFailure, Machine
from .models import IssueFilterAssociated, TestsuiteRun, UnknownFailure
from .models import TestResult, RunConfig, TextStatus, Test

from collections import namedtuple, OrderedDict, defaultdict
from dateutil.relativedelta import relativedelta, MO

from datetime import timedelta

from copy import deepcopy

import dateutil
import hashlib
import copy
import json
import csv
import io


class Period:
    def __init__(self, start, end, label_format='%Y-%m-%d %H:%M:%S'):
        self.start = start
        self.end = end

        self.start_label = start.strftime(label_format)
        self.end_label = end.strftime(label_format)

    def __repr__(self):
        return "{}->{}".format(self.start_label, self.end_label)

    def __str__(self):
        return repr(self)

    def __eq__(self, other):
        return self.start == other.start and self.end == other.end


class Periodizer:
    @classmethod
    def from_json(cls, json_string):
        params = json.loads(json_string)

        count = int(params.get('count', 30))

        end_date_str = params.get('end')
        if end_date_str is not None:
            end_date = timezone.make_aware(dateutil.parser.parse(end_date_str))
        else:
            end_date = timezone.now()

        period_str = params.get('period', 'week')
        if period_str == 'month':
            period_offset = relativedelta(months=1, day=1, hour=0, minute=0,
                                          second=0, microsecond=0)
            period = relativedelta(months=1)
            real_end = end_date + period_offset
            description = 'last {} months'.format(count, )
            if end_date_str is not None:
                description += " before {}".format(real_end.strftime("%B %Y"))
            label_format = "%b %Y"
        elif period_str == 'day':
            period_offset = relativedelta(days=1, hour=0, minute=0,
                                          second=0, microsecond=0)
            period = relativedelta(days=1)
            real_end = end_date + period_offset

            description = 'last {} days'.format(count)
            if end_date_str is not None:
                description += " before {}".format(real_end.date().isoformat())
            label_format = "%Y-%m-%d"
        else:  # fall-through: default to week
            period_offset = relativedelta(days=1, hour=0, minute=0,
                                          second=0, microsecond=0,
                                          weekday=MO(1))
            period = relativedelta(weeks=1)
            real_end = end_date + period_offset
            description = 'last {} weeks'.format(count)
            if end_date_str is not None:
                description += " before {} / {}".format(real_end.date().isoformat(),
                                                        real_end.strftime("WW-%Y.%W"))
            label_format = "WW-%Y-%W"

        return cls(period_offset=period_offset, period=period, period_count=count,
                   end_date=end_date, description=description, label_format=label_format)

    def __init__(self, period_offset=relativedelta(days=1, hour=0, minute=0,
                                                   second=0, microsecond=0,
                                                   weekday=MO(1)),
                 period=relativedelta(weeks=1), period_count=30,
                 end_date=timezone.now(),
                 description="last 30 weeks",
                 label_format="WW-%Y-%W"):
        self.period_offset = period_offset
        self.period = period
        self.period_count = period_count
        self.end_date = end_date
        self.description = description
        self.label_format = label_format

        self.end_cur_period = end_date + period_offset

    def __iter__(self):
        # Reset the current position
        self.cur_period = self.period_count
        return self

    def __next__(self):
        if self.cur_period == 0:
            raise StopIteration

        self.cur_period -= 1
        cur_time = self.end_cur_period - self.cur_period * self.period
        return Period(cur_time - self.period, cur_time, self.label_format)


PeriodOpenItem = namedtuple("PeriodOpenItem", ('period', 'label', 'active', 'new', 'closed'))


PeriodCommentItem = namedtuple("PeriodCommentItem", ('period', 'label', 'dev_comments', 'user_comments', 'accounts'))


class ItemCountTrend:
    def __init__(self, items, fields=[], periodizer=None):
        self.items = items
        self.periodizer = periodizer

        self.fields = defaultdict(list)
        for i in items:
            for field in fields:
                values = getattr(i, field)
                if not isinstance(values, str):
                    values = len(values)
                self.fields[field].append(values)

    @property
    def stats(self):
        r = {}
        if self.periodizer is not None:
            r["period_desc"] = self.periodizer.description
        for field, values in self.fields.items():
            r[field] = values
        return r


class OpenCloseCountTrend(ItemCountTrend):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, fields=['label', 'active', 'new', 'closed'], **kwargs)


class BugCommentCountTrend(ItemCountTrend):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, fields=['label', 'dev_comments', 'user_comments'], **kwargs)


def bugs_followed_since():
    bt = BugTracker.objects.exclude(components_followed_since=None).order_by('-components_followed_since').first()
    if bt is not None:
        return bt.components_followed_since
    else:
        return None


week_period = Periodizer.from_json('{"period": "week", "count":30}')


# TODO: Add tests for all these functions

def metrics_issues_over_time(periodizer=None, user_filters={}):
    if periodizer is None:
        periodizer = copy.copy(week_period)

    # Get a list of periods, from oldest to newest
    issues = []
    for period in periodizer:
        issues.append(PeriodOpenItem(period, period.start_label, [], [], []))

    if len(issues) == 0:
        return OpenCloseCountTrend(issues, periodizer=periodizer)

    query = Issue.from_user_filters(**user_filters).objects.exclude(archived_on__lt=issues[0].period.start)
    filtered_issues = query.exclude(added_on__gt=issues[-1].period.end)

    for i in filtered_issues:
        for ip in issues:
            if i.added_on >= ip.period.start and i.added_on < ip.period.end:
                ip.new.append(i)
            elif i.archived_on is not None and i.archived_on >= ip.period.start and i.archived_on < ip.period.end:
                ip.closed.append(i)
            elif i.added_on < ip.period.start and (i.archived_on is None or i.archived_on >= ip.period.end):
                ip.active.append(i)

    return OpenCloseCountTrend(issues, periodizer=periodizer)


def metrics_bugs_over_time(periodizer=None,
                           user_filters={}):
    if periodizer is None:
        periodizer = copy.copy(week_period)

    # Find out what is the earliest components_followed_since and make sure we don't go that far
    earliest_followed_since = bugs_followed_since()
    if earliest_followed_since is None:
        return (OpenCloseCountTrend([], periodizer=periodizer), BugCommentCountTrend([], periodizer=periodizer))

    # Get a list of periods, from oldest to newest
    bug_periods = []
    comment_periods = []
    for period in periodizer:
        # Ignore all the periods not fully covered
        if period.start < earliest_followed_since:
            continue
        bug_periods.append(PeriodOpenItem(period, period.start_label, [], [], []))
        comment_periods.append(PeriodCommentItem(period, period.start_label, [], [], None))

    followed_bug_ids = set()
    filtered_bugs = Bug.from_user_filters(**user_filters).objects.exclude(closed__lt=earliest_followed_since)
    for bug in filtered_bugs.prefetch_related("tracker"):
        # ignore bugs that we are not following
        if bug.component not in bug.tracker.components_followed_list:
            continue

        for bp in bug_periods:
            if bug.created >= bp.period.start and bug.created < bp.period.end:
                bp.new.append(bug)
            elif bug.closed is not None and bug.closed >= bp.period.start and bug.closed < bp.period.end:
                bp.closed.append(bug)
            elif bug.created < bp.period.start and (bug.closed is None or bug.closed >= bp.period.end):
                bp.active.append(bug)

        # Keep track of all the bugs we used for the open/close count
        followed_bug_ids.add(bug.id)

    # Fetch all comments made on the followed bugs and order them in chronological
    # order, before associating them to their corresponding bug period
    idx = 0
    followed_comments = BugComment.objects.filter(bug_id__in=followed_bug_ids,
                                                  created_on__gte=comment_periods[0].period.start,
                                                  created_on__lte=periodizer.end_cur_period).order_by('created_on')
    for comment in followed_comments.prefetch_related('account'):
        cp = comment_periods[idx]

        # go to the next period if the comment has been made after the end of the current period
        while comment.created_on > cp.period.end:
            idx += 1
            cp = comment_periods[idx]

        if comment.account.is_developer:
            cp.dev_comments.append(comment)
        else:
            cp.user_comments.append(comment)

    return (OpenCloseCountTrend(bug_periods, periodizer=periodizer),
            BugCommentCountTrend(comment_periods, periodizer=periodizer))


def metrics_comments_over_time(periodizer=None, user_filters={}):
    if periodizer is None:
        periodizer = Periodizer.from_json('{"period": "week", "count": 8}')

    earliest_followed_since = bugs_followed_since()
    if earliest_followed_since is None:
        return BugCommentCountTrend([], periodizer=periodizer), {}

    # Get a list of periods, from oldest to newest
    comment_periods = []
    for period in periodizer:
        # Ignore all the periods not fully covered
        if period.start < earliest_followed_since:
            continue
        period = PeriodCommentItem(period, period.start_label, [], [], defaultdict(list))
        comment_periods.append(period)

    if len(comment_periods) == 0:
        return BugCommentCountTrend([], periodizer=periodizer), {}

    # Get the list of comments wanted by the user, excluding comments made before the moment we started polling
    query = BugComment.from_user_filters(**user_filters).objects
    query = query.exclude(created_on__lt=comment_periods[0].period.start)
    filtered_comments = query.exclude(created_on__gt=comment_periods[-1].period.end)

    idx = 0
    cp = comment_periods[idx]
    accounts_found = set()
    for comment in filtered_comments.prefetch_related('bug__tracker', 'account__person').order_by('created_on'):
        # ignore comments on bugs that we are not following
        if comment.bug.component not in comment.bug.tracker.components_followed_list:
            continue

        # go to the next period if the comment has been made after the end of the current period
        while comment.created_on > cp.period.end:
            idx += 1
            cp = comment_periods[idx]

        # Add the comment to the global counter
        if comment.account.is_developer:
            cp.dev_comments.append(comment)
        else:
            cp.user_comments.append(comment)

        # Add the comment to the per-account list
        accounts_found.add(comment.account)
        cp.accounts[comment.account].append(comment)

    # Create an dictionnary that keeps track of comments per account
    sorted_account = sorted(accounts_found, key=lambda a: str(a))
    per_account_periods = OrderedDict()
    for account in sorted_account:
        per_account_periods[account] = []
        for cp in comment_periods:
            per_account_periods[account].append(cp.accounts[account])

    return BugCommentCountTrend(comment_periods, periodizer=periodizer), per_account_periods


class Bin:
    def __init__(self, upper_limit, label):
        self.items = set()
        self.upper_limit = upper_limit
        self.label = label


class TimeBinizer:
    def __init__(self, items, bins=[Bin(timedelta(hours=1), "under an hour"),
                                    Bin(timedelta(hours=6), "under 6 hours"),
                                    Bin(timedelta(days=1), "under a day"),
                                    Bin(timedelta(days=7), "under a week"),
                                    Bin(timedelta(days=30), "under a month"),
                                    Bin(timedelta(days=90), "under 3 months"),
                                    Bin(timedelta(days=365), "under a year"),
                                    Bin(timedelta.max, "over a year")]):
        self._bins = deepcopy(bins)
        for item, time in items:
            for bin in self._bins:
                if time < bin.upper_limit:
                    bin.items.add(item)
                    break

    @property
    def bins(self):
        return self._bins

    @property
    def stats(self):
        return {"items_count": [len(b.items) for b in self.bins],
                "label": [b.label for b in self.bins]}


def metrics_issues_ttr(date=timezone.now(), period=timedelta(days=30)):
    request = Issue.objects.filter(archived_on__lt=date, archived_on__gt=date-period).exclude(archived_on__isnull=True)
    return TimeBinizer([(item, item.archived_on - item.added_on) for item in request])


def metrics_open_issues_age(date=timezone.now()):
    request = Issue.objects.filter(archived_on=None)
    now = timezone.now()
    return TimeBinizer([(item, now - item.added_on) for item in request])


def metrics_failure_filing_delay(date=timezone.now(), period=timedelta(days=30)):
    request = KnownFailure.objects.filter(manually_associated_on__gt=date-period)
    request = request.filter(result__ts_run__runconfig__temporary=False)
    bins = [Bin(timedelta(hours=1), "under an hour"),
            Bin(timedelta(hours=8), "under 8 hours"),
            Bin(timedelta(days=1), "under a day"),
            Bin(timedelta(days=3), "under three days"),
            Bin(timedelta(days=7), "under a week"),
            Bin(timedelta(days=30), "under a month"),
            Bin(timedelta.max, "over a month")]
    return TimeBinizer([(item, item.filing_delay) for item in request], bins=bins)


def metrics_bugs_ttr(date=timezone.now(), period=timedelta(days=30), user_filters={}):
    request = Bug.from_user_filters(**user_filters).objects.filter(closed__lt=date, closed__gt=date-period)
    request = request.exclude(closed__isnull=True).prefetch_related("tracker")
    bugs = set(filter(lambda b: b.component in b.tracker.components_followed_list, request))
    return TimeBinizer([(item, item.closed - item.created) for item in bugs])


def metrics_open_bugs_age(date=timezone.now(), user_filters={}):
    request = Bug.from_user_filters(**user_filters).objects.filter(closed=None).prefetch_related("tracker")
    bugs = set(filter(lambda b: b.component in b.tracker.components_followed_list, request))
    now = timezone.now()
    return TimeBinizer([(item, now - item.created) for item in bugs])


class PieChartData:
    # results needs to be a dictionary mapping a label (string) to a number
    def __init__(self, results, colors=dict()):
        self._results = OrderedDict()
        self._colors = colors

        for label, value in sorted(results.items(), key=lambda kv: kv[1], reverse=True):
            self._results[label] = value

    def label_to_color(self, label):
        color = self._colors.get(label)

        if color is not None:
            return color
        else:
            blake2 = hashlib.blake2b()
            blake2.update(label.encode())
            return "#" + blake2.hexdigest()[-7:-1]

    @property
    def colors(self):
        return [self.label_to_color(label) for label in self._results.keys()]

    def stats(self):
        return {'results': list(self._results.values()),
                'labels': list(self._results.keys()),
                'colors': list(self.colors)}


class ColouredObjectPieChartData(PieChartData):
    def __init__(self, objects):
        results = defaultdict(int)
        for obj in objects:
            results[str(obj)] += 1

        colors = {}
        for obj in set(objects):
            colors[str(obj)] = obj.color

        super().__init__(results, colors)


def metrics_testresult_statuses_stats(results):
    return ColouredObjectPieChartData([r.status for r in results])


def metrics_knownfailure_statuses_stats(failures):
    return ColouredObjectPieChartData([f.result.status for f in failures])


def metrics_testresult_machines_stats(results):
    return ColouredObjectPieChartData([r.ts_run.machine for r in results])


def metrics_knownfailure_machines_stats(failures):
    return ColouredObjectPieChartData([f.result.ts_run.machine for f in failures])


def metrics_testresult_tests_stats(results):
    tests = {}
    for result in results:
        label = str(result.test)
        tests[label] = tests.get(label, 0) + 1
    return PieChartData(tests)


def metrics_knownfailure_tests_stats(failures):
    return metrics_testresult_tests_stats([f.result for f in failures])


def metrics_knownfailure_issues_stats(failures):
    # Since the query has likely already been made, we can't prefetch the
    # issues anymore... so let's hand roll it!
    matched_ifas = set()
    for failure in failures:
        matched_ifas.add(failure.matched_ifa_id)
    ifa_to_issues = dict()
    ifas = IssueFilterAssociated.objects.filter(id__in=matched_ifas)
    for e in ifas.prefetch_related('issue', 'issue__bugs', 'issue__bugs__tracker'):
        ifa_to_issues[e.id] = e.issue

    issues = {}
    for failure in failures:
        label = str(ifa_to_issues.get(failure.matched_ifa_id, None))
        issues[label] = issues.get(label, 0) + 1
    return PieChartData(issues)


def metrics_testresult_issues_stats(failures):
    total = []
    for failure in failures:
        total.extend(failure.known_failures.all())
    return metrics_knownfailure_issues_stats(total)


class Rate:
    def __init__(self, count, total):
        self.count = count
        self.total = total

    @property
    def percent(self):
        if self.total > 0:
            return self.count / self.total * 100.0
        else:
            return 0.0

    def __repr__(self):
        return "Rate({}, {})".format(self.count, self.total)

    def __str__(self):
        return "{:.2f}% ({} / {})".format(self.percent, self.count, self.total)


class LineChartData:
    # results needs to be a dictionary mapping a label (string) to a number
    def __init__(self, results, x_labels, line_label_colors={}):
        self._results = OrderedDict()
        self.x_labels = x_labels
        self.line_label_colors = line_label_colors

        for label, value in results.items():
            self._results[label] = value

    def label_to_color(self, label):
        blake2 = hashlib.blake2b()
        blake2.update(label.encode())
        default = "#" + blake2.hexdigest()[-7:-1]

        return self.line_label_colors.get(label, default)

    def stats(self):
        dataset = []
        for line_label, result in self._results.items():
            color = self.label_to_color(line_label)
            entry = {'label': line_label,
                     'borderColor': color,
                     'backgroundColor': color,
                     'data': result}
            dataset.append(entry)

        return {'dataset': dataset, 'labels': self.x_labels}


class MetricPassRatePerRunconfig:
    filtering_model = TestResult

    def _queryset_to_dict(self, Model, ids, *prefetch_related):
        return dict((o.pk, o) for o in Model.objects.filter(id__in=ids).prefetch_related(*prefetch_related))

    def __init__(self, user_filters):
        self.user_filters = user_filters

        self.query = self.filtering_model.from_user_filters(**user_filters)
        db_results = self.query.objects.values_list('id', 'status_id', 'ts_run__runconfig_id')

        # Collect all the failures
        statuses = self._queryset_to_dict(TextStatus, [r[1] for r in db_results], 'testsuite')
        failure_status_ids = set([s.id for s in statuses.values() if s.is_failure])
        failures = set()
        for result_id, status_id, runconfig_id in db_results:
            if status_id in failure_status_ids:
                failures.add(result_id)
        del failure_status_ids

        # Find the related issues
        known_failures = KnownFailure.objects.filter(result__in=failures).values_list('result_id',
                                                                                      'matched_ifa__issue_id',
                                                                                      'matched_ifa__issue__expected')
        issues = self._queryset_to_dict(Issue, [r[1] for r in known_failures],
                                        'bugs__tracker', 'bugs__assignee__person')
        self._issue_hit_count = defaultdict(int)    # [issue] = int
        expected_failures = set()  # [failure_ids](issues)
        for result_id, issue_id, issue_expected in known_failures:
            issue = issues[issue_id]
            if issue_expected:
                expected_failures.add(result_id)
            else:
                self._issue_hit_count[issue] += 1
        self.total_result_count = len(db_results)
        self.kept_result_count = self.total_result_count - len(expected_failures)
        del known_failures

        # Count the results per runconfig and statuses
        runconfigs = self._queryset_to_dict(RunConfig, [r[2] for r in db_results])
        runconfigs_tmp = defaultdict(lambda: defaultdict(int))
        statuses_tmp = set()
        for result_id, status_id, runconfig_id in db_results:
            if result_id not in expected_failures:
                status = statuses[status_id]
                runconfigs_tmp[runconfigs[runconfig_id]][status] += 1
                statuses_tmp.add(status)
        del expected_failures

        # Order the statuses and runconfigs
        runconfigs_ordered = sorted(runconfigs_tmp.keys(), key=lambda r: r.added_on, reverse=True)
        statuses_ordered = sorted(statuses_tmp, key=lambda r: str(r))

        # Create the final result structures
        self.runconfigs = OrderedDict()   # [runconfig][status] = Rate()
        self.statuses = OrderedDict()     # [status][runconfig] = Rate()
        self.results_count = dict()       # [runconfig] = int
        for status in statuses_ordered:
            self.statuses[status] = OrderedDict()
        for runconfig in runconfigs_ordered:
            # Compute the total for the run
            total = 0
            for status in statuses_ordered:
                total += runconfigs_tmp[runconfig][status]
            self.results_count[runconfig] = total

            # Add the passrate to both the runconfig-major and status-major table
            self.runconfigs[runconfig] = OrderedDict()
            for status in statuses_ordered:
                passrate = Rate(runconfigs_tmp[runconfig][status], total)
                self.runconfigs[runconfig][status] = self.statuses[status][runconfig] = passrate

    @property
    def discarded_rate(self):
        return Rate(self.total_result_count - self.kept_result_count, self.total_result_count)

    @cached_property
    def chart(self):
        # The runconfigs are ordered from newest to oldest, reverse that
        runconfigs = list(reversed(self.runconfigs.keys()))

        chart_data = defaultdict(list)
        for runconfig in runconfigs:
            for status in self.runconfigs[runconfig]:
                passrate = self.runconfigs[runconfig][status]
                chart_data[status.name].append(format((passrate.count / passrate.total) * 100, '.3f'))

        status_colors = dict()
        for status in self.statuses:
            status_colors[status.name] = status.color

        return LineChartData(chart_data, [r.name for r in runconfigs], status_colors)

    @cached_property
    def to_csv(self):
        f = io.StringIO()
        writer = csv.writer(f, quoting=csv.QUOTE_NONNUMERIC)

        writer.writerow(['Run Config', 'Results count'] + [str(s) for s in self.statuses])
        for runconfig, statuses in self.runconfigs.items():
            writer.writerow([runconfig, self.results_count[runconfig]] + [p.count for status, p in statuses.items()])

        return f.getvalue()

    @cached_property
    def most_hit_issues(self):
        ordered_issues = OrderedDict()
        for issue in sorted(self._issue_hit_count, key=lambda i: self._issue_hit_count[i], reverse=True):
            occ_count = self._issue_hit_count[issue]
            ordered_issues[issue] = Rate(occ_count, self.kept_result_count)
        return ordered_issues


class MetricPassRatePerTest:
    filtering_model = TestResult

    discarded_label = 'discarded (expected)'
    discarded_color = '#cc99ff'

    class AggregatedTestResults:
        def __init__(self, test):
            self.test = test

            self.results_count = 0
            self.statuses = defaultdict(int)
            self.issues = defaultdict(int)
            self.overall_result = self.test.testsuite.notrun_status
            self.worst_failures = []

            self.discarded_results_count = 0
            self.discarded_count_per_issue = defaultdict(int)  # [issue] = count

        def add_result(self, result_id, status, issues=None):
            self.results_count += 1
            self.statuses[status] += 1
            if issues is not None:
                for issue in issues:
                    self.issues[issue] += 1

            if self.overall_result is None or status.actual_severity > self.overall_result.actual_severity:
                self.overall_result = status
                self.worst_failures = []

            if status == self.overall_result:
                self.worst_failures.append(result_id)

        def add_discarded_result(self, result_id, issues=None):
            self.discarded_results_count += 1
            for issue in issues:
                self.discarded_count_per_issue[issue] += 1

        def status_occurence_rate(self, status):
            return Rate(self.statuses.get(status, 0), self.results_count)

        def issue_occurence_rate(self, issue):
            return Rate(self.issues.get(issue, 0), self.results_count)

        @property
        def rate_of_worst_failure(self):
            return self.status_occurence_rate(self.overall_result)

        @property
        def is_fully_discarded(self):
            return self.results_count == 0 and self.discarded_results_count > 0

        @property
        def issue_occurence_rates(self):
            rates = dict()
            if self.is_fully_discarded:
                for issue in self.discarded_count_per_issue:
                    rates[issue] = Rate(self.discarded_count_per_issue.get(issue, 0),
                                        self.results_count + self.discarded_results_count)
            else:
                for issue in self.issues:
                    rates[issue] = self.issue_occurence_rate(issue)
            return rates

        @cached_property
        def is_pass(self):
            # Notruns are not pass
            if self.overall_result.is_notrun:
                return False

            return not self.overall_result.is_failure

    PassRateStatistics = namedtuple('PassRateStatistics', 'passrate runrate discarded_rate notrun_rate')

    def _queryset_to_dict(self, Model, ids, *prefetch_related):
        return dict((o.pk, o) for o in Model.objects.filter(id__in=ids).prefetch_related(*prefetch_related))

    def __init__(self, user_filters):
        self.user_filters = user_filters
        self.query = self.filtering_model.from_user_filters(**user_filters)

        # Create the final result structures
        self.tests = OrderedDict()     # [test] = AggregatedTestResults()
        self.statuses = OrderedDict()  # [status] = Rate()

        # Fetch the user-filtered from the database
        db_results = self.query.objects.values_list('id', 'test_id', 'status_id', 'ts_run_id')

        # This query will lazily be evaluated, so it can be set early
        ts_runs = TestsuiteRun.objects.filter(id__in=[r[3] for r in db_results]).values_list('machine_id',
                                                                                             'runconfig_id')

        # Collect all the failures and ts_run_ids that got some notruns
        statuses = self._queryset_to_dict(TextStatus, [r[2] for r in db_results], 'testsuite')
        failure_status_ids = set([s.id for s in statuses.values() if s.is_failure])
        notrun_status_ids = set([s.id for s in statuses.values() if s.is_notrun])
        interrupted_tsruns_notruns_count = defaultdict(int)
        notrun_count = 0
        failure_ids = set()
        for result_id, test_id, status_id, ts_run_id in db_results:
            if status_id in failure_status_ids:
                failure_ids.add(result_id)
            elif status_id in notrun_status_ids:
                interrupted_tsruns_notruns_count[ts_run_id] += 1
                notrun_count += 1
        del failure_status_ids

        # Find the last test of every interrupted ts_run
        q = TestResult.objects.filter(ts_run__in=interrupted_tsruns_notruns_count.keys())
        q = q.exclude(status_id__in=notrun_status_ids).values_list('ts_run__id').annotate(last_test_run_id=Max('id'))
        last_result_of_tsr = set([v[1] for v in q])
        q2 = KnownFailure.objects.filter(result_id__in=last_result_of_tsr).values_list('result__ts_run_id',
                                                                                       'matched_ifa__issue_id')
        run_to_issue = dict(q2)

        id_to_issue = dict([(i.id, i) for i in Issue.objects.filter(id__in=run_to_issue.values())])
        issue_not_run_impact = defaultdict(int)
        for run_id, issue_id in run_to_issue.items():
            issue = id_to_issue[issue_id]
            notruns_count = interrupted_tsruns_notruns_count.pop(run_id)
            issue_not_run_impact[issue] += notruns_count

        undocumented_failure_not_run_count = 0
        q3 = UnknownFailure.objects.filter(result_id__in=last_result_of_tsr).values_list('result__ts_run_id', flat=True)
        for undocumented_interrupted_ts_run_id in set(q3):
            notruns_count = interrupted_tsruns_notruns_count.pop(undocumented_interrupted_ts_run_id)
            undocumented_failure_not_run_count += notruns_count

        unexplained_notruns_count = sum(interrupted_tsruns_notruns_count.values())
        del run_to_issue
        del id_to_issue
        del interrupted_tsruns_notruns_count

        # Find the related issues
        known_failures = KnownFailure.objects.filter(result__in=failure_ids).values_list('result_id',
                                                                                         'matched_ifa__issue_id',
                                                                                         'matched_ifa__issue__expected')
        issues = self._queryset_to_dict(Issue, [r[1] for r in known_failures],
                                        'bugs__tracker', 'bugs__assignee__person')
        issue_hit_count = defaultdict(int)    # [issue] = Rate
        failures = defaultdict(set)           # [result_id] = set(issues)
        expected_failures = defaultdict(set)  # [failure_ids](issues)
        explained_failure_ids = set()
        for result_id, issue_id, issue_expected in known_failures:
            issue = issues[issue_id]
            if issue_expected:
                expected_failures[result_id].add(issue)
            else:
                failures[result_id].add(issue)
                issue_hit_count[issue] += 1
            explained_failure_ids.add(result_id)
        self.total_result_count = len(db_results)
        self.kept_result_count = self.total_result_count - len(expected_failures)
        self.uncovered_failure_rate = Rate(len(failure_ids - explained_failure_ids), len(failure_ids))
        self.notrun_rate = Rate(notrun_count, self.total_result_count)
        del known_failures
        del failure_ids
        del explained_failure_ids

        # Create all the aggregated results for each test, in the alphabetical order
        tests = self._queryset_to_dict(Test, [r[1] for r in db_results], 'testsuite')
        for test in sorted(tests.values(), key=lambda t: str(t)):
            self.tests[test] = self.AggregatedTestResults(test)

        # Move all the results to their right category
        status_result_count = defaultdict(int)
        for result_id, test_id, status_id, machine_id in db_results:
            test = tests[test_id]

            if result_id not in expected_failures:
                status = statuses[status_id]
                status_result_count[status] += 1
                self.tests[test].add_result(result_id, status, failures.get(result_id))
            else:
                self.tests[test].add_discarded_result(result_id, expected_failures.get(result_id))

        # We do not need these results anymore, so free them
        del tests
        del failures
        del expected_failures
        del issues
        del db_results

        # Get the list of machines and runconfigs
        self.machines = Machine.objects.filter(id__in=[r[0] for r in ts_runs])
        self.runconfigs = RunConfig.objects.filter(id__in=[r[1] for r in ts_runs])

        # Compute the results-level stats
        self.most_hit_issues = OrderedDict()
        for issue in sorted(issue_hit_count.keys(), key=lambda i: issue_hit_count[i], reverse=True):  # noqa
            self.most_hit_issues[issue] = Rate(issue_hit_count[issue], self.kept_result_count)
        del issue_hit_count

        self.most_interrupting_issues = OrderedDict()
        self.explained_interruption_rate = Rate(0, self.notrun_rate.count)
        for issue in sorted(issue_not_run_impact.keys(), key=lambda i: issue_not_run_impact[i], reverse=True):  # noqa
            rate = Rate(issue_not_run_impact[issue], self.notrun_rate.count)
            self.most_interrupting_issues[issue] = rate
            self.explained_interruption_rate.count += rate.count
        self.unknown_failure_interruption_rate = Rate(undocumented_failure_not_run_count, self.notrun_rate.count)
        self.unexplained_interruption_rate = Rate(unexplained_notruns_count, self.notrun_rate.count)

        # Create all the status rates in alphabetical order and compute their occurence rate
        statuses_ordered = sorted(status_result_count.keys(), key=lambda r: str(r))  # only the statuses we kept
        for status in statuses_ordered:
            self.statuses[status] = Rate(status_result_count[status], self.kept_result_count)

    @cached_property
    def result_statuses_chart(self):
        statuses = {}
        colors = {}
        for status, rate in self.statuses.items():
            label = str(status)
            statuses[label] = rate.count
            colors[label] = status.color

        discarded_count = self.total_result_count - self.kept_result_count
        if discarded_count > 0:
            statuses[self.discarded_label] = discarded_count
            colors[label] = self.discarded_color

        return PieChartData(statuses, colors)

    @cached_property
    def uncovered_failure_rate_chart(self):
        statuses = {
            'filed / documented': self.uncovered_failure_rate.total - self.uncovered_failure_rate.count,
            'need filing': self.uncovered_failure_rate.count,
        }

        colors = {
            'filed / documented': '#33cc33',
            'need filing': '#ff0000',
        }

        return PieChartData(statuses, colors)

    @cached_property
    def aggregated_statuses_chart(self):
        statuses = defaultdict(int)
        colors = {}

        for test, agg_result in self.tests.items():
            if agg_result.is_fully_discarded:
                label = self.discarded_label
            else:
                label = str(agg_result.overall_result)
            statuses[label] += 1

        colors[self.discarded_label] = self.discarded_color
        for status in self.statuses:
            colors[str(status)] = status.color

        return PieChartData(statuses, colors)

    @cached_property
    def passrate_chart(self):
        stats = self.statistics

        statuses = {
            'pass rate': stats.passrate.count,
            'fail rate': stats.runrate.count - stats.passrate.count,
            'discarded rate': stats.discarded_rate.count,
            'notrun rate': stats.notrun_rate.count,
        }

        colors = {
            'pass rate': '#33cc33',
            'fail rate': '#ff0000',
            'discarded rate': self.discarded_color,
            'notrun rate': '#000000',
        }

        return PieChartData(statuses, colors)

    @cached_property
    def statistics(self):
        passrate = Rate(0, len(self.tests))
        runrate = Rate(0, len(self.tests))
        discarded_rate = Rate(0, len(self.tests))
        notrun_rate = Rate(0, len(self.tests))

        for test, results in self.tests.items():
            if not results.overall_result.is_notrun:
                runrate.count += 1
                if not results.overall_result.is_failure:
                    passrate.count += 1
                elif results.is_fully_discarded:
                    discarded_rate.count += 1
            else:
                notrun_rate.count += 1

        return self.PassRateStatistics(passrate, runrate, discarded_rate, notrun_rate)


class MetricRuntimeHistory:
    filtering_model = TestsuiteRun

    def _queryset_to_dict(self, Model, ids, *prefetch_related):
        return dict((o.pk, o) for o in Model.objects.filter(id__in=ids).prefetch_related(*prefetch_related))

    def __init__(self, user_filters, average_per_machine=False):
        self.user_filters = user_filters
        self.average_per_machine = average_per_machine

        self.query = self.filtering_model.from_user_filters(**user_filters)

        # Fetch the user-filtered from the database
        filtered_results = self.query.objects
        filtered_results = filtered_results.filter(duration__gt=timedelta(seconds=0))  # Ignore negative exec times
        db_results = filtered_results.values('id', 'runconfig_id',
                                             'machine_id').annotate(total=Sum("duration"))

        # Get the runconfigs and machines
        runconfigs = self._queryset_to_dict(RunConfig, [r['runconfig_id'] for r in db_results])
        machines = self._queryset_to_dict(Machine, [r['machine_id'] for r in db_results], "aliases")
        self._tsr_ids = [r['id'] for r in db_results]

        # Add the results to the history, then compute the totals
        self.participating_machines = defaultdict(lambda: defaultdict(set))
        runconfigs_tmp = defaultdict(lambda: defaultdict(timedelta))
        machines_tmp = set()
        for r in db_results:
            # de-duplicate shard machines by replacing a machine by its shard
            machine = machines[r['machine_id']]
            if machine.aliases is not None:
                machine = machine.aliases

            runconfigs_tmp[runconfigs[r['runconfig_id']]][machine] += r['total']
            self.participating_machines[runconfigs[r['runconfig_id']]][machine].add(r['machine_id'])
            machines_tmp.add(machine)

        # Order the machines and runconfigs
        runconfigs_ordered = sorted(runconfigs_tmp.keys(), key=lambda r: r.added_on, reverse=True)
        machines_ordered = sorted(machines_tmp, key=lambda m: str(m))

        # Create the final result structures
        self.runconfigs = OrderedDict()   # [runconfig][machine] = Rate()
        self.machines = OrderedDict()     # [machine][runconfig] = Rate()
        for machine in machines_ordered:
            self.machines[machine] = OrderedDict()
        for runconfig in runconfigs_ordered:
            # Add the runtime to both the runconfig-major and machine-major table
            self.runconfigs[runconfig] = OrderedDict()
            for machine in machines_ordered:
                total_time = runconfigs_tmp[runconfig][machine]
                machine_count = len(self.participating_machines[runconfig][machine])
                if average_per_machine:
                    if machine_count > 0:
                        actual_time = total_time / machine_count
                    else:
                        actual_time = timedelta()
                else:
                    actual_time = total_time
                self.runconfigs[runconfig][machine] = self.machines[machine][runconfig] = actual_time

    def _machine_to_name(self, machine):
        counts = list()
        for runconfig in self.runconfigs:
            counts.append(len(self.participating_machines[runconfig][machine]))

        c_min = min(counts)
        c_max = max(counts)

        if c_max == 1:
            return str(machine)
        elif c_min == c_max:
            return "{} ({})".format(str(machine), c_min)
        else:
            return "{} ({}-{})".format(str(machine), c_min, c_max)

    @cached_property
    def chart(self):
        # The runconfigs are ordered from newest to oldest, reverse that
        runconfigs = list(reversed(self.runconfigs.keys()))

        chart_data = defaultdict(list)
        for runconfig in runconfigs:
            for machine in self.runconfigs[runconfig]:
                time = self.runconfigs[runconfig][machine]
                if time != timedelta():
                    minutes = format(time.total_seconds() / 60, '.2f')
                else:
                    minutes = "null"
                chart_data[self._machine_to_name(machine)].append(minutes)

        machines_colors = dict()
        for machine in self.machines:
            machines_colors[self._machine_to_name(machine)] = machine.color

        return LineChartData(chart_data, [r.name for r in runconfigs], machines_colors)

    @cached_property
    def longest_tests(self):
        TestRunTime = namedtuple("TestRunTime", ('test', 'machine', 'min', 'avg', 'max'))
        longest_tests = []

        db_results = TestResult.objects.filter(ts_run_id__in=self._tsr_ids)
        db_results = db_results.values("test_id", "ts_run__machine_id")
        db_results = db_results.annotate(d_avg=Avg("duration"),
                                         d_min=Min("duration"),
                                         d_max=Max("duration")).order_by("-d_max")
        db_results = db_results[:1000]

        tests = self._queryset_to_dict(Test, [r['test_id'] for r in db_results], "testsuite")
        machines = self._queryset_to_dict(Machine, [r['ts_run__machine_id'] for r in db_results], "aliases")

        # Create all the TestRunTime objects
        aliased_machines = defaultdict(list)
        for r in db_results:
            test = tests[r['test_id']]
            machine = machines[r['ts_run__machine_id']]
            runtime = TestRunTime(test, machine, r['d_min'], r['d_avg'], r['d_max'])

            if machine.aliases is None:
                longest_tests.append(runtime)
            else:
                aliased_machines[(test, machine.aliases)].append(runtime)

        # Deduplicate all the data for aliased machines
        for key, results in aliased_machines.items():
            test, machine = key

            # Aggregate all the values (Avg won't be super accurate, but good-enough)
            d_min = results[0].min
            avg_sum = timedelta()
            d_max = results[0].max
            for result in results:
                d_min = min(d_min, result.min)
                d_max = max(d_max, result.max)
                avg_sum += result.avg

            longest_tests.append(TestRunTime(results[0].test, machine, d_min, avg_sum / len(results), d_max))

        return sorted(longest_tests, key=lambda r: r.max, reverse=True)[:100]
