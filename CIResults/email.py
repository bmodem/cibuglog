from django.conf import settings
from django.core.mail import send_mail


class Email:
    def __init__(self, subject, message, to):
        self.subject = subject.strip()
        self.message = message
        self.to = to

    def send(self):
        send_mail(self.subject, self.message, settings.EMAIL_ADDRESS, self.to)
