/*
 * Adds a row to the tbody of a table identified by an id (#mytable) that has
 * an attribute data-row-next-id set,.
 *
 * WARNING: if data-row-next-id is set to 0, then the tbody is cleared before
 *          adding anything. This is useful to add placeholders.
 */
function table_add_row(table_id) {
    // get an id for the new row, and increment the storage
    var id = $(table_id).data("row-next-id")
    $(table_id).data("row-next-id", id + 1)

    // Remove the current content (placeholder) if the the id is 0
    if (id == 0)
        $(table_id+" tbody").html("");

    // Create a new row
    row = $("<tr></tr>")
    row.data('id', id)

    // Move the row to the right location
    $(table_id+" tbody").append(row)

    return row
}

function handleRemoveRow() {
    $('[data-remove-row]').click(function() {
        $(this).closest("tr").remove();
    });
}

function handleEditFilter() {
    $("[data-edit-filter]").click(function(click){
        var select=$($($(click.target).parents()[1]).find("select"))
        $(window).trigger('issue-edit-filter', select.val());
    });
}

function makeDualListBox(name) {
    var filter_tags_select = $('select[name="'+name+'"]').bootstrapDualListbox({
        nonSelectedListLabel: 'Ignored',
        selectedListLabel: 'Selected',
        moveOnSelect: false,
        selectorMinimalHeight: 175,
        bootstrap3Compatible: true,
    });
}

function dualListBoxSelected(name) {
    var ret = [];
    var objs = $('[name="'+name+'"]').val();
    for (var o in objs)
        ret.push(objs[o]);
    return ret;
}

function arrayToInt(array) {
    for (var i in array)
        array[i] = parseInt(array[i], 10);
    return array;
}

function dualListBoxReset(name) {
    $('[name="'+name+'"]').val([])
    $('[name="'+name+'"]').bootstrapDualListbox('refresh')
}

function do_background_post_request(url, attributes) {
    var formData = new FormData();
    formData.append("csrfmiddlewaretoken", $("[name=csrfmiddlewaretoken]").val());
    for(var key in attributes) {
        formData.append(key, attributes[key]);
    }

    var request = new XMLHttpRequest();
    request.open("POST", url, false);
    request.send(formData);

    return request
}

function handleConfirmations() {
    var current = null;

    /* Find all the href with data-confirmation */
    $('a[data-confirmation]').click(function() {
        current = this
        eModal.confirm($(this).data("confirmation"), null).then(function() {
            req = do_background_post_request(current.href, {});

            if (req.status == 200) {
                if ($(current).data('confirmation-reload') !== undefined) {
                    location.reload();
                }
            } else if ($(current).data('confirmation-msg-on-error') !== undefined) {
                alert($(current).data("confirmation-msg-on-error"), "Error");
            }
            current = null;
        });
        return false;
    });

    /* For forms */
    $('button[data-submit-confirm]').click(function() {
        current_form = $(this).closest("form")
        checked = $(current_form).find('input[type="checkbox"]:checked').length

        msg = $(this).data("submit-confirm")
        msg = msg.replace("{checked}", checked)

        eModal.confirm(msg, null).then(function() {
            $(current_form).submit()
        });
        return false;
    });
}

function handleInforms() {
    /* Find all the href with data-confirmation */
    $('a[data-inform]').click(function() {
        eModal.alert($(this).data("inform"), "Information");
        return false;
    });

    /* Find all the href with data-confirmation */
    $('a[data-inform-target]').click(function() {
        eModal.alert($("#" + $(this).data("inform-target")).html(), "Information");
        return false;
    });
}

function handleSelectAll() {
    /* Add a checkbox */
    $('[data-select-name]').each(function(){
        $(this).prepend('<span class="select-item"><input type="checkbox" name="' + $(this).data("select-name") + '"/></span>');
    });

    $('a[data-select-all]').click(function() {
        $("#" + $(this).data("select-all")).find("input").prop('checked', true);
        return false;
    });

    $('a[data-unselect-all]').click(function() {
        $("#" + $(this).data("unselect-all")).find("input").prop('checked', false);
        return false;
    });
}

function handleDualListBox() {
    $('[data-dual-list-box]').each(function() {
        makeDualListBox($(this).attr('name'));
    })
}

function setAccountIsDev(account_id, is_dev) {
    var xhr = new XMLHttpRequest();

    // FIXME: Find a way not to hardcode the URL here...
    xhr.open('PATCH', "../api/bugtrackeraccount/" + account_id + "/");
    xhr.setRequestHeader('X-CSRFToken', '{{ csrf_token }}');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.onload = function() {
        if (xhr.status === 200) {
            location.reload();
        }
    };
    xhr.send(JSON.stringify({"is_developer": is_dev}));
}

function handleAccountEdit() {
    /* Find all the href with data-confirmation */
    $('a[data-account-edit]').click(function() {
        var account_id = $(this).data("account-edit")

        var options = {
            message: 'What role should this account have?',
            title: "Account role selection",
            size: eModal.size.sm,
            buttons: [
                {text: 'Cancel', style: 'default',   close: true, click: function() {
                    // Nothing to do
                }
                },
                {text: 'User', style: 'success',   close: true, click: function() {
                        setAccountIsDev(account_id, false);
                    }
                },
                {text: 'Dev', style: 'primary',   close: true, click: function() {
                        setAccountIsDev(account_id, true);
                    }
                },
            ],
        };

        eModal.alert(options);
        return false;
    });
}

function formDisableOnSubmit() {
    $('form[data-disable-on-submit').submit(function() {
        txt = $(this).data('disable-on-submit')
        $("[type='submit']", this)
            .html(txt)
            .attr('disabled', 'disabled');

        return true;
    });
}

function handleSetAsQuery() {
    var queryInput = $('#userFilteringQuery');
    $('a[data-set-as-query]').click(function() {
        queryInput.attr('value', $(this).data("set-as-query"))
    })

    var datalist = $('#' + queryInput.attr('list'));
    var queryKeywords = [];
    datalist.children('option').each(function() {
        queryKeywords.push($(this).text());
    });
    queryInput.on('input', function() {
        var parts = $(this).val().split(' ');
        var lastWord = parts[parts.length - 1];
        // If we're trying to autocomplete an empty word, don't suggest
        // anything. Wait for the user to type at least one letter to avoid
        // confusing suggestions.
        if (lastWord == '') {
            datalist.empty();
            return;
        }
        var prefix = parts.slice(0, -1).join(' ');
        var options = [];
        for (var i = 0; i < queryKeywords.length; i++) {
            var keyword = queryKeywords[i];
            if (keyword.indexOf(lastWord) == 0) {
                options.push($('<option></option>').text(prefix+' '+keyword));
            }
        }
        datalist.html(options);
    }).trigger('input');
}

function str_to_csv_value(string) {
    return '"' + string.replace('"', '""').trim() + '"'
}

function handleHideableColumns() {
    $('div.column-selector').each(function() {
        var selector = $(this);
        var class_name = selector.data('table-class');

        // Get all the columns and check for consistancy
        var columns = [];
        $('table.' + class_name).each(function() {
            var table_columns = $(this).find('th').map(function() {
                return {
                    name: $(this).text(),
                    visibility: $(this).data('visibility')
                };
            });

            if (columns.length == 0) {
                columns = table_columns;
            } else {
                var inconsistent = false;
                if (columns.length == table_columns.length) {
                    for (var i = 0; i < columns.length; i++) {
                        if (columns[i].text != table_columns[i].text ||
                            columns[i].visibility != table_columns[i].visibility)
                            inconsistent = true;
                    }
                } else
                    inconsistent = true;

                if (inconsistent)
                    console.warn('Table with class "' + class_name + '" have inconsistent header');
            }
        });

        // Read the user preferences
        var storage_id = 'table_columns__' + class_name;
        var preferences = JSON.parse(localStorage.getItem(storage_id));
        if (preferences === null)
            preferences = {}

        // Set the columns in the selector
        var html = '<label for="userFilteringQuery">Show columns</label>\n';
        for (var i = 0; i < columns.length; i++) {
            var col_show = preferences[columns[i].name];

            var attrs = '';
            if (columns[i].visibility == 'always')
                attrs += 'checked disabled ';
            else if (col_show || (typeof col_show === 'undefined' && columns[i].visibility == 'show'))
                attrs += 'checked ';

            html += '<div class="checkbox"><label><input type="checkbox" data-td-offset="' + (i + 1) + '"' + attrs + '> ' + columns[i].name + '</label></div>\n';
        }

        var download_as = selector.data('table-downloadable-as');
        if (download_as != null) {
            html += '<div class="checkbox"><a class="" href="#" download='+download_as+' title="Download as CSV" data-table-as-csv-class='+class_name+'><span class="glyphicon glyphicon-download-alt"/> CSV</a></div>'

            $(this).html(html);

            selector.find('a[download]').click(function() {
                // TODO: handle the case where some columns are hidden
                var headers = [];
                for (var i = 0; i < columns.length; i++) {
                    headers.push(str_to_csv_value(columns[i].name));
                }

                var rows = [];
                $('table.' + class_name).find('tr').each(function() {
                    var cols = [];
                    $(this).find('td').each(function() {
                        cols.push(str_to_csv_value($(this).text().trim()));
                    });

                    if (cols.length == headers.length)
                        rows.push(cols);
                });

                // Write the data as CSV
                var csv = headers.join() + "\n";
                for (var i = 0; i < rows.length; i++) {
                    csv += rows[i].join() + "\n"
                }

                var blob = new Blob([csv], {type:"text/csv;charset=utf-8"})
                var fname = $(this).attr("download") || "table.csv"
                saveAs(blob, fname)

                return false;
            })
        } else {
            $(this).html(html);
        }

        // Now make sure we hide the columns we don't want, and react to changes
        show_hide = function() {
            var col_to_hide = $(this).data('td-offset');

            objs = $('table.' + class_name).find("td:nth-of-type(" + col_to_hide + "), th:nth-of-type(" + col_to_hide + ")")

            var column = $.trim($(this).parent().text());
            preferences[column] = this.checked;
            if(this.checked)
                objs.show()
            else
                objs.hide()

            // Store the new preferences
            localStorage.setItem(storage_id, JSON.stringify(preferences));
        };
        selector.find('input').each(show_hide);
        selector.find('input').change(show_hide);
    })
}

/* Handle all the start-up scripts */
$(document).ready(function() {
    handleRemoveRow();

    handleConfirmations();

    handleInforms();

    handleSelectAll();

    handleDualListBox();

    handleAccountEdit();

    formDisableOnSubmit();

    handleSetAsQuery();

    handleHideableColumns();
})
