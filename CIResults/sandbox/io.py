import os
import sys
import json
import array
import struct
import subprocess
from threading import RLock
from subprocess import PIPE
from types import ModuleType

# Make sure the sandbox module is available
sys.path.append(os.path.dirname(os.path.normpath(__file__)))
from lockdown import LockDown   # noqa


class IOWrapper:
    # Commands
    EXEC_USER_SCRIPT = "exec_user_script"
    CALL_USER_FUNCTION = "call_user_function"

    def __init__(self, stream_in=None, stream_out=None):
        self.stream_in = stream_in if stream_in else sys.stdin.buffer
        self.stream_out = stream_out if stream_out else sys.stdout.buffer

    def send(self, data):
        if not isinstance(data, bytes):
            data = str(data).encode()
        length = len(data)
        header = array.array('b', b'\0\0\0\0')  # 32 bit length
        struct.pack_into(">L", header, 0, length)
        self.stream_out.write(header.tobytes() + data)
        self.stream_out.flush()

    def read(self):
        try:
            header = self.stream_in.read(4)
            length = struct.unpack(">L", header)[0]
            content = self.stream_in.read(length)

            if len(content) != length:
                raise IOError("The message read is shorter than expected")

            return content.decode()
        except struct.error:
            raise IOError("Invalid message format")


class Server:
    def __init__(self, stream_in=None, stream_out=None, lockdown=True):
        self.iowrapper = IOWrapper(stream_in=stream_in, stream_out=stream_out)
        self.usr_module = None

        # NOTE: The code is covered, but since we have to run it in a separate
        # process, the coverage does not pick it up
        if lockdown:
            from seccomplite import ALLOW, EQ, Arg                                       # pragma: no cover

            ld = LockDown()                                                              # pragma: no cover
            ld.add_rule(ALLOW, "read", Arg(0, EQ, self.iowrapper.stream_in.fileno()))    # pragma: no cover
            ld.add_rule(ALLOW, "write", Arg(0, EQ, self.iowrapper.stream_out.fileno()))  # pragma: no cover
            ld.add_rule(ALLOW, "write", Arg(0, EQ, sys.stderr.fileno()))                 # pragma: no cover
            ld.start()                                                                   # pragma: no cover

    def serve_request(self):
        rpc_call = json.loads(self.iowrapper.read())
        try:
            # NOTE: All the callabable methods are prefixed with rpc__
            func = getattr(self, "rpc__" + rpc_call['method'])
            ret = func(rpc_call['kwargs'], rpc_call['version'])
            self.iowrapper.send(json.dumps(ret))
        except AttributeError:
            self.iowrapper.send(json.dumps({'return_code': 404}))

    def serve_forever(self):
        while True:                                           # pragma: no cover
            self.serve_request()                              # pragma: no cover

    # ----- RPC methods (need to be prefixed with 'rpc__') -----

    def rpc__exec_user_script(self, kwargs, version):
        # remove the current user module to allow the users to send new scripts
        # without restarting the service.
        del self.usr_module

        user_script = kwargs.get('script')
        if user_script is None:
            return {'return_code': 400, 'reason': "The 'script' parameter is missing"}

        # Create a dynamic module
        self.usr_module = ModuleType('usr_script')
        try:
            code = compile(user_script, "<user script>", 'exec')
            exec(code,  self.usr_module.__dict__)
        except Exception as e:
            return {'return_code': 400, 'reason': 'Compilation error: {}'.format(e)}

        return {'return_code': 200}

    def rpc__call_user_function(self, kwargs, version):
        if 'user_function' not in kwargs or 'user_kwargs' not in kwargs:
            return {'return_code': 400,
                    'reason': "Both the 'user_function' and 'user_kwargs' parameters are needed"}

        try:
            func = getattr(self.usr_module, kwargs['user_function'])
        except AttributeError:
            return {'return_code': 404, 'reason': 'The user function does not exist'}

        try:
            ret = func(**kwargs['user_kwargs'])
            return {'return_code': 200, 'return_value': ret}
        except Exception as e:
            return {'return_code': 400, 'reason': str(e)}


class Client:
    _instance_cache = dict()

    class UserFunctionCallError(Exception):
        def __init__(self, return_code, reason):
            self.return_code = return_code
            self.reason = reason

        def __str__(self):
            return "UserFunctionCallError {}: {}".format(self.return_code, self.reason)

    @classmethod
    def get_or_create_instance(cls, script):
        instance = cls._instance_cache.get(script)
        if instance is None:
            instance = cls._instance_cache[script] = cls(script)
        return instance

    @property
    def interpreter(self):
        """
        Intuitively, 'sys.executable' would be used for this purpose, but when using
        uwsgi, it clobbers the actual interpreter pointed to by sys.executable, and
        replaces it with the path to the uwsgi executable...because that makes sense.

        One workaround would be to use a shebang, but that won't work, as the env is wiped
        when starting the server process. So the PATH variable won't reflect the updated
        PATH when using a venv (either locally or in the Docker container).

        `which python3` works, as it will return the path to the interpreter based on the
        PATH variable. This works whether using a venv locally or in the docker container,
        as it is executed separately in the current env.

        NOTE: This is not 100% foolproof. If the user is not using the same interpreter
        for execution as is pointed to in their PATH var, then this could call a different
        interpreter. That would be bad practice, but it could happen.

        If following the standard setup guide, or using Docker containers, this
        works perfectly fine though. There could be room for improvement here, as uwsgi
        maintainers are well aware of the 'sys.executable' issue and don't seem to care.
        """
        if hasattr(self, '_interpreter'):
            return self._interpreter

        interp = subprocess.check_output(["which", "python3"])
        self._interpreter = interp.decode().strip()
        return self._interpreter

    def shutdown(self):
        if hasattr(self, "sesh"):
            self.sesh.kill()

        try:
            del self._instance_cache[self.usr_script]
        except KeyError:
            pass  # Already garbage collected

    def _restart_server(self):
        self.shutdown()

        self.sesh = subprocess.Popen([self.interpreter, __file__], stdin=PIPE, stdout=PIPE,
                                     universal_newlines=False, env={})
        self.iowrapper = IOWrapper(stream_in=self.sesh.stdout, stream_out=self.sesh.stdin)

        ret = self.rpc_call(self.iowrapper.EXEC_USER_SCRIPT, {'script': self.usr_script}, retry=False)
        if ret.get('return_code') != 200:
            raise ValueError('Error {} - Cannot set the user script: {}'.format(ret.get('return_code'),
                                                                                ret.get('reason')))

    def __init__(self, usr_script):
        self.usr_script = usr_script

        self.lock = RLock()
        self._restart_server()

    def __del__(self):
        self.shutdown()

    def rpc_call(self, method, kwargs=None, version=1, retry=True):
        if kwargs is None:
            kwargs = {}

        # Make sure we do not have multiple RPC calls at the same time
        with self.lock:
            # Try up to 3 times to make the call, and restart the server after each fail
            for i in range(3):
                try:
                    self.iowrapper.send(json.dumps({'method': method, 'kwargs': kwargs, 'version': version}))
                    return json.loads(self.iowrapper.read())
                except Exception:
                    pass

                # restart the server, since we got an unexpected output
                if retry:
                    self._restart_server()
                else:
                    break

            raise IOError("Failed to make an RPC call: method='{}', kwargs={}. version={}".format(method,
                                                                                                  kwargs,
                                                                                                  version))

    def call_user_function(self, func_name, kwargs):
        ret = self.rpc_call(self.iowrapper.CALL_USER_FUNCTION, {"user_function": func_name, "user_kwargs": kwargs})
        if ret.get('return_code') == 200:
            return ret.get('return_value')
        else:
            raise Client.UserFunctionCallError(ret.get('return_code'), ret.get('reason'))


# If the script is run directly, just start a server
if __name__ == "__main__":
    # This code is tested by test_sandbox.IntegrationTests, but coverage cannot access line coverage there
    __io_s = Server(lockdown=True)       # pragma: no cover
    __io_s.serve_forever()               # pragma: no cover
