from unittest import skipIf
from unittest.mock import patch, MagicMock
from django.test import TestCase

from CIResults.sandbox.io import IOWrapper, Server, Client
from CIResults.sandbox.lockdown import LockDown

from io import BytesIO
import json
import sys
import os


def create_pipe():
    p_in, p_out = os.pipe()
    return os.fdopen(p_in, 'rb'), os.fdopen(p_out, 'wb')


class IOWrapperTests(TestCase):
    MSG = "Some string with non-ascii characters - éè"

    def test_streams__default_values(self):
        wrapper = IOWrapper()
        self.assertEqual(wrapper.stream_in, sys.stdin.buffer)
        self.assertEqual(wrapper.stream_out, sys.stdout.buffer)

    def test_streams__overriden(self):
        p_in, p_out = create_pipe()
        wrapper = IOWrapper(p_in, p_out)

        self.assertEqual(wrapper.stream_in, p_in)
        self.assertEqual(wrapper.stream_out, p_out)

    def test_send_then_read__byte_array(self):
        p_in, p_out = create_pipe()
        wrapper = IOWrapper(p_in, p_out)

        wrapper.send(self.MSG.encode())
        self.assertEqual(wrapper.read(), self.MSG)

    def test_send_then_read__string(self):
        p_in, p_out = create_pipe()
        wrapper = IOWrapper(p_in, p_out)

        wrapper.send(self.MSG)
        self.assertEqual(wrapper.read(), self.MSG)

    def test_read__header_too_short(self):
        wrapper = IOWrapper(BytesIO(b'hel'))
        self.assertRaisesMessage(IOError, "Invalid message format", wrapper.read)

    def test_read__message_too_short(self):
        wrapper = IOWrapper(BytesIO(b'hello world'))
        self.assertRaisesMessage(IOError, "The message read is shorter than expected", wrapper.read)


class ServerTests(TestCase):
    def setUp(self):
        self.pc_r, self.pc_w = create_pipe()  # For the client -> server communication
        self.ps_r, self.ps_w = create_pipe()  # For server -> client communication

        # Now create a server, and don't forget to swap TX and RX!
        self.client = IOWrapper(self.ps_r, self.pc_w)
        self.server = Server(self.pc_r, self. ps_w, lockdown=False)

    def test_init(self):
        self.assertEqual(self.server.iowrapper.stream_in, self.pc_r)
        self.assertEqual(self.server.iowrapper.stream_out, self. ps_w)

    def test_serve_request__not_a_json(self):
        self.client.send("hello world")
        self.assertRaises(json.decoder.JSONDecodeError, self.server.serve_request)

    def _do_request(self, request):
        self.client.send(json.dumps(request))
        self.server.serve_request()
        return json.loads(self.client.read())

    def test_serve_request__non_existing_method(self):
        ret = self._do_request({'method': 'missing', 'kwargs': {}, 'version': 1})
        self.assertEqual(ret, {'return_code': 404})

    def test_rpc__set_user_script__invalid_syntax(self):
        ret = self._do_request({'method': IOWrapper.EXEC_USER_SCRIPT, 'version': 1,
                                'kwargs': {'script': "gfdsgfdg"}})
        self.assertEqual(ret, {'return_code': 400, 'reason': "Compilation error: name 'gfdsgfdg' is not defined"})

    def test_rpc__set_user_script__missing_script(self):
        ret = self._do_request({'method': IOWrapper.EXEC_USER_SCRIPT, 'version': 1,
                                'kwargs': {}})
        self.assertEqual(ret, {'return_code': 400, 'reason': "The 'script' parameter is missing"})

    def _exec_usr_function(self, script, func_name, usr_kwargs={}):
        ret = self._do_request({'method': IOWrapper.EXEC_USER_SCRIPT, 'version': 1,
                                'kwargs': {'script': script}})
        self.assertEqual(ret, {'return_code': 200})

        # Now try calling the function
        ret = self._do_request({'method': IOWrapper.CALL_USER_FUNCTION, 'version': 1,
                                'kwargs': {'user_function': func_name, 'user_kwargs': usr_kwargs}})
        return ret

    def test_rpc__call_user_function__missing_rpc_parameters(self):
        ret = self._do_request({'method': IOWrapper.CALL_USER_FUNCTION, 'version': 1,
                                'kwargs': {}})
        self.assertEqual(ret, {'return_code': 400,
                               'reason': "Both the 'user_function' and 'user_kwargs' parameters are needed"})

    def test_rpc__call_user_function__unknown_function(self):
        ret = self._exec_usr_function("def helloworld(toto): pass", "invalid")
        self.assertEqual(ret, {'return_code': 404, 'reason': 'The user function does not exist'})

    def test_rpc__call_user_function__missing_argument(self):
        ret = self._exec_usr_function("def helloworld(toto): pass", "helloworld")
        self.assertEqual(ret, {'return_code': 400,
                               'reason': "helloworld() missing 1 required positional argument: 'toto'"})

    def test_rpc__call_user_function__success(self):
        ret = self._exec_usr_function("def func(): return 'called!'", "func")
        self.assertEqual(ret, {'return_code': 200, 'return_value': "called!"})

    # NOTE: Coverage with the Server in lockdown is provided by the integration test


@skipIf(not LockDown.is_supported(), "SECCOMP is missing")
class LockDownTests(TestCase):
    def test_make_coverage_happy(self):
        # This is already properly tested, but in a forked process which means
        # coverage does not get access to it
        from seccomplite import ALLOW, Filter

        ld = LockDown()
        ld.add_rule(ALLOW, "read")
        ld.f = Filter(def_action=ALLOW)
        ld.start()

    def _test_operation(self, method, with_lockdown=False):
        pid = os.fork()
        if pid == 0:
            if with_lockdown:                                 # pragma: no cover
                LockDown().start()                            # pragma: no cover
            method()                                          # pragma: no cover

            # exit immediately without calling any cleanup functions
            os._exit(0)                                       # pragma: no cover
        else:
            return os.waitpid(pid, 0)[1]

    # ---------- Operations that should fail in lockdown mode but succeed otherwise ----------
    def check_fail__read_file(self):
        with open("/etc/resolv.conf", "r") as f:              # pragma: no cover
            f.readlines()                                     # pragma: no cover

    def check_fail__write_file(self):
        with open("/tmp/foo", "w") as f:                      # pragma: no cover
            f.write("Short message")                          # pragma: no cover

    def check_fail__stat_file(self):
        os.stat('/etc/resolv.conf')                           # pragma: no cover

    def check_fail__reset_sandbox(self):
        from seccomplite import Filter, ALLOW                 # pragma: no cover
        f = Filter(def_action=ALLOW)                          # pragma: no cover
        f.load()                                              # pragma: no cover

    # ---------- Operations that should always succeed ----------
    def check_pass__big_alloc(self):
        x = "*" * 1000000                                     # pragma: no cover
        del x                                                 # pragma: no cover

    def check_pass__import_standard_library(self):
        import re                                      # noqa # pragma: no cover
        import sys                                     # noqa # pragma: no cover
        import os                                      # noqa # pragma: no cover

    def test_operations(self):
        for op_name in [o for o in dir(self) if o.startswith('check_')]:
            operation = getattr(self, op_name)

            if op_name.startswith('check_fail__'):
                with self.subTest(msg="Checking operation {}: free mode".format(op_name)):
                    self.assertEqual(self._test_operation(operation, False), 0)
                with self.subTest(msg="Checking operation {}: lockdown mode".format(op_name)):
                    self.assertNotEqual(self._test_operation(operation, True), 0)
            elif op_name.startswith('check_pass__'):
                with self.subTest(msg="Checking operation {}: lockdown mode".format(op_name)):
                    self.assertEqual(self._test_operation(operation, True), 0)


class UserFunctionCallErrorTests(TestCase):
    def test_exception(self):
        exc = Client.UserFunctionCallError(400, 'reason')

        self.assertEqual(exc.return_code, 400)
        self.assertEqual(exc.reason, 'reason')
        self.assertEqual(str(exc), "UserFunctionCallError 400: reason")


class ClientTests(TestCase):
    SCRIPT = "def helloworld(): return 'OK'"

    @patch('subprocess.Popen')
    @patch('subprocess.check_output')
    def setUp(self, chk_out_mock, popen_mocked):
        self.pc_r, self.pc_w = create_pipe()  # For the client -> server communication
        self.ps_r, self.ps_w = create_pipe()  # For server -> client communication

        self.server_io = IOWrapper(stream_in=self.pc_r, stream_out=self.ps_w)
        popen_mocked.return_value = MagicMock(stdout=self.ps_r, stdin=self.pc_w,
                                              kill=MagicMock())
        chk_out_mock.return_value = sys.executable.encode()

        # Pre-send a success for the first rpc call
        self.server_io.send('{"return_code": 200}')
        self.client = Client(self.SCRIPT)
        self.first_request = json.loads(self.server_io.read())

    @patch('CIResults.sandbox.io.Client.__init__', return_value=None)
    def test_get_or_create_instance(self, client_mocked):
        SCRIPT1 = 'hello'
        SCRIPT2 = 'hello2'

        script1 = Client.get_or_create_instance(SCRIPT1)
        self.assertEqual(Client.get_or_create_instance(SCRIPT1), script1)

        script2 = Client.get_or_create_instance(SCRIPT2)
        self.assertEqual(Client.get_or_create_instance(SCRIPT2), script2)

        self.assertNotEqual(script2, script1)

    def test_init_sequence(self):
        self.assertEqual(self.client.usr_script, self.SCRIPT)
        self.assertEqual(self.first_request,
                         {"method": "exec_user_script",
                          "kwargs": {"script": "def helloworld(): return 'OK'"},
                          "version": 1})

    def test_init_sequence_with_bad_script(self):
        RETURN_CODE = 101
        REASON = "Generic obscure reason"
        MSG = 'Error {} - Cannot set the user script: {}'.format(RETURN_CODE, REASON)

        self.client.rpc_call = MagicMock(return_value={'return_code': RETURN_CODE, 'reason': REASON})
        self.assertRaisesMessage(ValueError, MSG, self.client._restart_server)

    def test_rpc_call__retries(self):
        self.client._restart_server = MagicMock()

        for i in range(10):
            self.server_io.send('invalid {}'.format(i))
        self.assertRaisesMessage(IOError,
                                 "Failed to make an RPC call: method='remote_func', kwargs={}. version=1",
                                 self.client.rpc_call, 'remote_func')
        self.assertEqual(self.client._restart_server.call_count, 3)

    def test_rpc_call__retries_disabled(self):
        self.client._restart_server = MagicMock()

        for i in range(10):
            self.server_io.send('invalid {}'.format(i))
        self.assertRaisesMessage(IOError,
                                 "Failed to make an RPC call: method='remote_func', kwargs={}. version=1",
                                 self.client.rpc_call, 'remote_func', retry=False)
        self.assertEqual(self.client._restart_server.call_count, 0)

    def test_call_user_function__success(self):
        FUNC_NAME = 'custom'
        FUNC_ARGS = {'arg1': 'val1', 'arg2': 'val2'}
        FUNC_RETURN = 1234

        self.client.rpc_call = MagicMock(return_value={'return_code': 200, 'return_value': FUNC_RETURN})
        self.assertEqual(self.client.call_user_function(FUNC_NAME, FUNC_ARGS), FUNC_RETURN)
        self.client.rpc_call.assert_called_with(IOWrapper.CALL_USER_FUNCTION, {'user_function': FUNC_NAME,
                                                                               'user_kwargs': FUNC_ARGS})

    def test_call_user_function__failure(self):
        RETURN_CODE = 101
        REASON = "Generic obscure reason"

        self.client.rpc_call = MagicMock(return_value={'return_code': RETURN_CODE, 'reason': REASON})

        # make sure that the call raises, then make sure the exception is filled correctly
        self.assertRaises(Client.UserFunctionCallError, self.client.call_user_function, 'custom', {})
        try:
            self.client.call_user_function('custom', {})
        except Client.UserFunctionCallError as e:
            self.assertEqual(e.return_code, RETURN_CODE)
            self.assertEqual(e.reason, REASON)


class IntegrationTests(TestCase):
    def test_call_user_function(self):
        client = Client(ClientTests.SCRIPT)
        client.call_user_function('helloworld', {})
