FROM python:3.7

ARG VERSION=""
LABEL VERSION=${VERSION}

ENV DEBIAN_FRONTEND noninteractive

COPY . /app

RUN apt-get clean && apt-get update && apt-get -y install build-essential libcap-dev libseccomp-dev libgraphviz-dev
RUN pip install uwsgi
RUN pip install --no-cache-dir -r /app/requirements.txt

ENV CIBUGLOG_VERSION=${VERSION}

WORKDIR /app
CMD uwsgi /app/uwsgi-docker.ini
